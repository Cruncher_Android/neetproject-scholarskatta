import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CareerNotesPage } from './career-notes.page';

describe('CareerNotesPage', () => {
  let component: CareerNotesPage;
  let fixture: ComponentFixture<CareerNotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerNotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CareerNotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
